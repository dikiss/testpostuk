package com.example.testpostuk.api

import com.example.testpostuk.model.Post
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface PostApiClient {

    // Get list of articles as Observable
    @GET("posts")
    fun getPost(): Observable<List<Post>>

    // Get one article by it's id
    @GET("posts/{id}")
    fun getPost(@Path("id") id: Int): Observable<Post>

    // Add new post
    @Headers("Content-Type: application/json;charset=utf-8")
    @POST("posts")
    fun addPost(@Body post: Post): Observable<Post>

    companion object {

        fun create(): PostApiClient {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .build()

            return retrofit.create(PostApiClient::class.java)

        }
    }

}