package com.example.testpostuk

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.example.testpostuk.adapter.PostAdapter
import com.example.testpostuk.api.PostApiClient
import com.example.testpostuk.model.Post
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val client by lazy {
        PostApiClient.create()
    }

    var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showPost()
        showPost(1)
        val post = Post(1,101, "Test Post", "Have fun posting")
        postPost(post)

    }

    private fun showPost() {

        disposable = client.getPost()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> setupRecycler(result) },
                { error -> Log.e("ERROR", error.message) }
            )

    }

    private fun showPost(id: Int) {

        disposable = client.getPost(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> Log.v("POST ID ${id}: ", "" + result) },
                { error -> Log.e("ERROR", error.message) }
            )

    }

    private fun postPost(article: Post) {

        disposable = client.addPost(article)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> Log.v("POSTED POST", "" + article ) },
                { error -> Log.e("ERROR", error.message ) }
            )
    }

    fun setupRecycler(articleList: List<Post>) {
        articles_recycler.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        articles_recycler.layoutManager = layoutManager
        articles_recycler.adapter = PostAdapter(articleList){
            Log.v("Post", it.id.toString())
        }
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }

}