package com.example.testpostuk.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.testpostuk.R
import com.example.testpostuk.model.Post
import kotlinx.android.synthetic.main.card_layout.view.*



class PostAdapter(
    private val articleList: List<Post>,
    private val listener: (Post) -> Unit
): RecyclerView.Adapter<PostAdapter.PostHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PostHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.card_layout, parent, false))

    override fun onBindViewHolder(holder: PostHolder, position: Int) = holder.bind(articleList[position], listener)

    override fun getItemCount() = articleList.size

    class PostHolder(postView: View): RecyclerView.ViewHolder(postView) {

        fun bind(post: Post, listener: (Post) -> Unit) = with(itemView) {
            title.text = post.title
            body.text = post.body
            itemView.setOnClickListener { listener(post) }


        }
    }
}